var gulp = require('gulp');
var typescript = require('gulp-typescript');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('compile', function(){

	return gulp.src([
		'app.ts',
		'main.ts',
		'routes/**/*.ts'
	], { base: './' })
		.pipe(sourcemaps.init())
		.pipe(typescript({ target: "ES5", removeComments: true }))
		.js
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./'));

});

gulp.task('build', function () {

	return gulp.src([
		'config/platform/logs.json',
		'config/default.js',
		'logs/*',
		'models/**/*.js',
		'models/**/*.json',
		'public/**/*.*',
		'routes/**/*.js',
		'views/**/*.pug',
		'app.js',
		'patch.js',
		'package.json',
		'htdigest',
		'cluster.json'
	], { base: './', allowEmpty: true })
		.pipe(gulp.dest('product'));

});

gulp.task('default',gulp.series('compile','build'), function(){

});
