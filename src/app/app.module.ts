/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */

"use strict";

import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import {HttpClientModule} from "@angular/common/http";
import { FlexLayoutModule } from "@angular/flex-layout";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";



import {
	MatButtonModule,
	MatButtonToggleModule,
	MatCardModule,
	MatIconModule,
	MatListModule, MatMenuModule,
	MatProgressSpinnerModule,
	MatSidenavModule, MatSliderModule, MatSnackBarModule, MatTabsModule,
	MatToolbarModule,
} from "@angular/material";

import {AppRoutingModule} from "./app-routing.module";
import { AppComponent } from "./app.component";
import {ContentModule} from "./content/content.module";

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		HttpClientModule,
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		FlexLayoutModule,

		MatTabsModule,
		MatCardModule,
		MatListModule,
		MatIconModule,
		MatButtonModule,
		MatSnackBarModule,
		MatToolbarModule,
		MatSidenavModule,
		MatSliderModule,
		MatMenuModule,
		MatButtonToggleModule,
		MatProgressSpinnerModule,

		ContentModule,
	],
	bootstrap: [AppComponent],
})
export class AppModule { }
