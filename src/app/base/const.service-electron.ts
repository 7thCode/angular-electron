/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */

"use strict";

import {Injectable} from "@angular/core";

/**
 * HTTP
 *
 * @since 0.01
 */

@Injectable({
	providedIn: "root",
})

export class ConstService {

	public endPoint: string;
	public winsockAddress: string;

	constructor() {
		this.endPoint = "http://seventh-code.com:3000";
		this.winsockAddress = "ws://seventh-code.com:5001";
	}

}
