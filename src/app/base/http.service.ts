/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */

"use strict";

import {HttpClient, HttpHeaders} from "@angular/common/http";

export interface IErrorObject {
	code: number;
	message: string;
}

export type Callback<T> = (error: IErrorObject | null, results: T | null) => void;

/**
 * HTTP
 *
 * @since 0.01
 */
export abstract class HttpService {

	protected httpOptions: any;
	protected networkError: any;

	constructor(protected http: HttpClient) {
		this.httpOptions = {
			headers: new HttpHeaders({
				"Accept": "application/json; charset=utf-8",
				"Content-Type": "application/json; charset=utf-8",
				"x-requested-with": "XMLHttpRequest",
			}),
		};
		this.networkError = {code: 10000, message: "network error"};
	}

}
