/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */

"use strict";

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import {ContentComponent} from "./content/content.component";

const routes: Routes = [
	{path: "", component: ContentComponent},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})

export class AppRoutingModule { }
