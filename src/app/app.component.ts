/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */

"use strict";

import {MediaMatcher} from "@angular/cdk/layout";
import {ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {MatSidenav} from "@angular/material";
import {ActivatedRoute} from "@angular/router";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.css"],
})

export class AppComponent implements OnInit, OnDestroy {

	public mobileQuery: MediaQueryList;
	protected mobileQueryListener: () => void;

	@ViewChild("sidenav", {static: false}) private sidenav: MatSidenav;

	public menu: string = "";

	constructor(private route: ActivatedRoute, public change: ChangeDetectorRef, protected media: MediaMatcher) {
		this.mobileQuery = media.matchMedia("(max-width: 600px)");
		this.mobileQueryListener = () => change.detectChanges();
		this.mobileQuery.addListener(this.mobileQueryListener);
	}

	public ngOnInit() {

	}

	public ngOnDestroy(): void {
		this.mobileQuery.removeListener(this.mobileQueryListener);
	}

	public close(opened) {
		if (opened) {
			this.sidenav.close().then(() => {

			});
		}
	}

}
