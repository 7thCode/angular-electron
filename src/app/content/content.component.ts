/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */


"use strict";

import {HttpClient} from "@angular/common/http";
import {Component, OnInit} from "@angular/core";
import {ElectronService} from "ngx-electron";
import {ConstService} from "../base/const.service";
import {ContentService} from "./content.service";

@Component({
	selector: "app-content",
	templateUrl: "./content.component.html",
	styleUrls: ["./content.component.css"],
})

export class ContentComponent implements OnInit {

	public title = "";
	public service: ContentService;

	constructor(protected http: HttpClient, private electronService: ElectronService, public constService: ConstService) {
		this.service = new ContentService(http, electronService, constService);
	}

	public ngOnInit() {
		this.service.get("hoge", (error, value) => {
			this.title = value;
		});
	}

}
