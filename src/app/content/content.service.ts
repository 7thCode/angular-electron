/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */

"use strict";

import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {ElectronService} from "ngx-electron";
import {retry} from "rxjs/operators";

import {ConstService} from "../base/const.service";
import {HttpService} from "../base/http.service";

export interface IErrorObject {
	code: number;
	message: string;
}

export type Callback<T> = (error: IErrorObject | null, results: T | null) => void;

@Injectable({
	providedIn: "root",
})

export class ContentService extends HttpService {

	constructor(public http: HttpClient, private electron: ElectronService, public constService: ConstService) {
		super(http);
	}

	public query(query: any, option: any, callback: Callback<any[]>): void {
		const queryString: string = encodeURIComponent(JSON.stringify(query));
		const optionString: string = encodeURIComponent(JSON.stringify(option));
		this.http.get(this.constService.endPoint + "/app/query/" + queryString + "/" + optionString, this.httpOptions).pipe(retry(3)).subscribe((results: ArrayBuffer): void => {
			if (results) {
				if (Array.isArray(results)) {
					const filterd = [];
					results.forEach((result) => {
						filterd.push(result);
					});
					callback(null, filterd);
				} else {
					callback({code: -1, message: "error"}, null);
				}
			} else {
				callback(this.networkError, null);
			}
		}, (error: HttpErrorResponse): void => {
			callback({code: -1, message: error.message}, null);
		});
	}

	public count(query: any, callback: Callback<number>): void {
		const queryString: string = encodeURIComponent(JSON.stringify(query));
		this.http.get(this.constService.endPoint + "/app/count/" + queryString, this.httpOptions).pipe(retry(3)).subscribe((result: any): void => {
			if (result) {
				callback(null, result);
			} else {
				callback(this.networkError, 0);
			}
		}, (error: HttpErrorResponse): void => {
			callback({code: -1, message: error.message}, null);
		});
	}

	public get(id: string, callback: Callback<any>): void {
		if (this.electron.isElectronApp) {
			this.electron.ipcRenderer.on("reply", (event: any, _result: string): void => {
				try {
					const result = JSON.parse(_result);
					if (result.code === 0) {
						callback(null, result.value);
					} else {
						callback(result, null);
					}
				} catch (e) {
					callback(this.networkError, null);
				}
			});
			this.electron.ipcRenderer.send("/app", id);
		} else {
			this.http.get(this.constService.endPoint + "/app/" + encodeURIComponent(id), this.httpOptions).pipe(retry(3)).subscribe((result: any): void => {
				if (result) {
					if (result.code === 0) {
						callback(null, result.value);
					} else {
						callback(result, null);
					}
				} else {
					callback(this.networkError, null);
				}
			}, (error: HttpErrorResponse): void => {
				callback({code: -1, message: error.message}, null);
			});
		}
	}

}
