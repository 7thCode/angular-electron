import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatCardModule} from "@angular/material";
import { NgxElectronModule } from "ngx-electron";
import {ContentComponent} from "./content.component";
import {ContentService} from "./content.service";

@NgModule({
	declarations: [ContentComponent],
	imports: [
		CommonModule,
		MatCardModule,
		NgxElectronModule,
	],
	providers: [
		ContentService,
	],
})

export class ContentModule {
}
