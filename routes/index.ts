/**
 * Copyright (c) 2019 7thCode.(http://seventh-code.com/)
 * This software is released under the MIT License.
 * opensource.org/licenses/mit-license.php
 */

"use strict";

namespace Index {

	const {ipcMain} = require("electron");

	const transform = (v: string): string => {
		return v;
	};

	if (ipcMain) { // electron routing

		ipcMain.on("/app", (event, arg): void => {
			event.sender.send("reply",  JSON.stringify({code: 0, value: transform(arg)}));
		});

	} else { // web routing

		const express: any = require("express");
		const router: any = express.Router();

		router.get("/app/:arg", (request, response, next): void => {
			response.json({code: 0, value: transform(request.params.arg)});
		});

		module.exports = router;
	}

}

