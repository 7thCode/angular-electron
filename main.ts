namespace Main {

	const {app, Menu, BrowserWindow} = require("electron");
	const url = require("url");
	const path = require("path");

	const router = require("./routes/index");

	let mainWindow: any = null;
	let childWindow: any = null;

	const createWindow: any = (): void => {
		mainWindow = new BrowserWindow({
			width: 800,
			height: 600,
			webPreferences: {
				nodeIntegration: true,
			},
		});

		childWindow = new BrowserWindow({parent: mainWindow, modal: true, show: false});
		childWindow.loadURL("https://github.com");

		mainWindow.loadURL(
			url.format({
				pathname: path.join(__dirname, "/dist/index.html"),
				protocol: "file:",
				slashes: true,
			}));

		// Open the DevTools.
		// mainWindow.webContents.openDevTools();

		mainWindow.on("closed", (): void => {
			mainWindow = null;
		});
	};

	app.on("ready", createWindow);

	app.on("window-all-closed", (): void => {
		if (process.platform !== "darwin") {
			app.quit();
		}
	});

	app.on("activate", (): void => {
		if (mainWindow === null) {
			createWindow();
		}
	});

	const templateMenu: any = [
		{
			label: "File",
			submenu: [
				{role: "quit"},
			],
		},
		{
			label: "Edit",
			submenu: [
				{role: "undo"},
				{role: "redo"},
			],
		},
		{
			label: "View",
			submenu: [
				{
					label: "Open DevTools",
					click: () => {
						mainWindow.webContents.openDevTools({ mode: "detach" });
					},
				},
				{
					label: "Learn More",
					click: () => {
						childWindow.show();
					},
				},
				{
					label: "Close",
					click: () => {
						childWindow.close();
					},
				},
			],
		},
	];

	const menu = Menu.buildFromTemplate(templateMenu);
	Menu.setApplicationMenu(menu);
}
